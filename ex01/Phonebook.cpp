#include <iostream>
#include <iomanip>
#include "Phonebook.h"

const Contact *Phonebook::getContacts() const
{
	return contacts;
}

int Phonebook::getNumContacts() const
{
	return numContacts;
}

int Phonebook::addContact(Contact c)
{
	if (numContacts > 7)
	{
		std::cout << "Can not add contact to phonebook\n";
		return 0;
	}
	contacts[numContacts++] = c;
	return 1;
}

int Phonebook::printHeader()
{
	std::cout << std::setfill(' ') << std::setw(11);
	std::cout << "Index|";
	std::cout << std::setfill(' ') << std::setw(11);
	std::cout << "First name|";
	std::cout << std::setfill(' ') << std::setw(11);
	std::cout << "Last name|";
	std::cout << std::setfill(' ') << std::setw(11);
	std::cout << "Nickname|";
	std::cout << std::endl;
	for (int i = 0; i < 4; i++)
	{
		std::cout << std::setfill('-') << std::setw(11);
		std::cout << "|";
	}
	std::cout << std::endl;
	return (1);
}

int Phonebook::searchContact()
{
	this->printHeader();
	for(int i = 0; i < numContacts; i++)
	{
		contacts[i].printTrunc(i + 1);
		std::cout << std::endl;
	}
	std::cout << std::endl;
	std::string str;
	int id(0);
	do
	{
		std::cout << "Enter index of contact: ";
		std::getline(std::cin, str);
		try
		{
			id = std::stoi(str, NULL);
		}
		catch (std::exception &ex)
		{
			id = 0;
		}
	}
	while (id < 1 || id > numContacts);
	std::cout << std::endl;
	contacts[id - 1].printAll();
	return (1);
}

Phonebook::Phonebook()
	: numContacts(0)
{ }
