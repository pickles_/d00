//
// Created by Dmitry Titenko on 10/30/17.
//

#ifndef D00_PHONEBOOK_H
#define D00_PHONEBOOK_H


#include "Contact.h"

class Phonebook
{
	public:
		Phonebook();

		const Contact *getContacts() const;

		int getNumContacts() const;

		int addContact(Contact c);
		int searchContact();
		int printHeader();


	private:
		Contact	contacts[8];
		int		numContacts;

	public:

};


#endif //D00_PHONEBOOK_H
