NAME = phonebook

CXX = clang++
CXX_FLAGS = -Wall -Wextra -Werror

RM = /bin/rm
MKDIR = /bin/mkdir
PRINTF = /usr/bin/printf
ECHO = /bin/echo

NO_COLOR =		\033[0;00m
OK_COLOR =		\033[38;5;02m
ERROR_COLOR =	\033[38;5;01m
WARN_COLOR =	\033[38;5;03m
SILENT_COLOR =	\033[38;5;04m

SRC_DIR = ./
OBJ_DIR = ./obj/
SRC =	Contact.cpp\
		Phonebook.cpp\
		main.cpp

OBJ = $(SRC:.cpp=.o)
SRC:= $(addprefix $(SRC_DIR), $(SRC))
OBJ:= $(addprefix $(OBJ_DIR), $(OBJ))

IFLAGS = -I.
LFLAGS =

.PHONY: all re clean fclean

all: $(NAME)

$(NAME): $(OBJ)
	@$(PRINTF) "$(SILENT_COLOR)./$(NAME) binary$(NO_COLOR)"
	@$(CXX) $(CFLAGS) $(IFLAGS) $(LFLAGS) -o $(NAME) $(OBJ)
	@$(PRINTF)	"\t[$(OK_COLOR)✓$(NO_COLOR)]$(NO_COLOR)\n"

$(OBJ_DIR)%.o:$(SRC_DIR)%.cpp
	@$(MKDIR) -p $(OBJ_DIR)
	@$(CXX) -c $(CXX_FLAGS) $(IFLAGS) $(LFLAGS) -o $@ $<
	@$(PRINTF) "$(OK_COLOR)✓ $(NO_COLOR)$<\n"

clean:
	@$(RM) -rf $(OBJ_DIR)
	@$(PRINTF) "$(SILENT_COLOR)$(NAME) : Cleaned Objects$(NO_COLOR)\n"

fclean: clean
	@$(RM) -rf $(NAME)
	@$(PRINTF) "$(SILENT_COLOR)$(NAME) : Cleaned Binary$(NO_COLOR)\n"

re: fclean all
