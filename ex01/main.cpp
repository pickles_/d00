#include "Phonebook.h"
#include <iostream>

int main()
{
	Phonebook phonebook;
	Contact contact;
	std::string cmd;

	while (!std::cin.eof())
	{
		std::cout << "Enter command: ";
		std::getline(std::cin, cmd);
		if (cmd == "EXIT")
			break;
		else if (cmd == "ADD")
		{
			if (phonebook.getNumContacts() == 8)
				std::cout << "Phonebook is full!" << std::endl;
			else
			{
				contact.requestAll();
				phonebook.addContact(contact);
			}
		}
		else if (cmd == "SEARCH")
		{
			if (phonebook.getNumContacts() > 0)
				phonebook.searchContact();
			else
				std::cout << "Phonebook is empty!" << std::endl;
		}
		std::cout << std::endl;
	}
	return (0);
}
