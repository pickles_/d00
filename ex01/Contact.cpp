//
// Created by Dmitry Titenko on 10/30/17.
//

#include <iostream>
#include <iomanip>
#include "Contact.h"

const std::string &Contact::getFirstName() const
{
	return firstName;
}

void Contact::setFirstName(const std::string &firstName)
{
	Contact::firstName = firstName;
}

const std::string &Contact::getLastName() const
{
	return lastName;
}

void Contact::setLastName(const std::string &lastName)
{
	Contact::lastName = lastName;
}

const std::string &Contact::getNickname() const
{
	return nickname;
}

void Contact::setNickname(const std::string &nickname)
{
	Contact::nickname = nickname;
}

const std::string &Contact::getLogin() const
{
	return login;
}

void Contact::setLogin(const std::string &login)
{
	Contact::login = login;
}

const std::string &Contact::getPostalAdress() const
{
	return postalAdress;
}

void Contact::setPostalAdress(const std::string &postalAdress)
{
	Contact::postalAdress = postalAdress;
}

const std::string &Contact::getEmail() const
{
	return email;
}

void Contact::setEmail(const std::string &email)
{
	Contact::email = email;
}

const std::string &Contact::getPhoneNumber() const
{
	return phoneNumber;
}

void Contact::setPhoneNumber(const std::string &phoneNumber)
{
	Contact::phoneNumber = phoneNumber;
}

const std::string &Contact::getBirthday() const
{
	return birthday;
}

void Contact::setBirthday(const std::string &birthday)
{
	Contact::birthday = birthday;
}

const std::string &Contact::getFavMeal() const
{
	return favMeal;
}

void Contact::setFavMeal(const std::string &favMeal)
{
	Contact::favMeal = favMeal;
}

const std::string &Contact::getUnderwearColor() const
{
	return underwearColor;
}

void Contact::setUnderwearColor(const std::string &underwearColor)
{
	Contact::underwearColor = underwearColor;
}

const std::string &Contact::getDarkestSecret() const
{
	return darkestSecret;
}

void Contact::setDarkestSecret(const std::string &darkestSecret)
{
	Contact::darkestSecret = darkestSecret;
}

Contact::Contact()
{}

void Contact::requestFirstName()
{
	std::cout << "First name: ";
	std::getline(std::cin, firstName);
}

void Contact::requestLastName()
{
	std::cout << "Last name: ";
	std::getline(std::cin, lastName);
}

void Contact::requestNickname()
{
	std::cout << "Nickname: ";
	std::getline(std::cin, nickname);
}

void Contact::requestLogin()
{
	std::cout << "Login: ";
	std::getline(std::cin, login);
}

void Contact::requestPostalAdress()
{
	std::cout << "Postal address: ";
	std::getline(std::cin, postalAdress);
}

void Contact::requestEmail()
{
	std::cout << "Email address: ";
	std::getline(std::cin, email);
}

void Contact::requestPhoneNumber()
{
	std::cout << "Phone number: ";
	std::getline(std::cin, phoneNumber);
}

void Contact::requestBirthday()
{
	std::cout << "Birthday date: ";
	std::getline(std::cin, birthday);
}

void Contact::requestFavMeal()
{
	std::cout << "Favorite meal: ";
	std::getline(std::cin, favMeal);
}

void Contact::requestUnderwearColor()
{
	std::cout << "Underwear color: ";
	std::getline(std::cin, underwearColor);
}

void Contact::requestDarkestSecret()
{
	std::cout << "Darkest secret: ";
	std::getline(std::cin, darkestSecret);
}

void Contact::requestAll()
{
	this->requestFirstName();
	this->requestLastName();
	this->requestNickname();
	this->requestLogin();
	this->requestPostalAdress();
	this->requestEmail();
	this->requestPhoneNumber();
	this->requestBirthday();
	this->requestFavMeal();
	this->requestUnderwearColor();
	this->requestDarkestSecret();
}

void Contact::printTrunc(int id)
{
	printTruncStr(std::to_string(id));
	std::cout << '|';
	printTruncStr(firstName);
	std::cout << '|';
	printTruncStr(lastName);
	std::cout << '|';
	printTruncStr(nickname);
	std::cout << '|';
}

void Contact::printTruncStr(const std::string &str)
{
	char buffer[11];
	std::cout << std::setfill(' ') << std::setw(10);
	if (str.size() > 10)
	{
		str.copy(buffer, 9);
		buffer[9] = '.';
		buffer[10] = '\0';
		std::cout << buffer;
	}
	else
		std::cout << str;

}

void Contact::printAll()
{
	std::cout << "First name: ";
	std::cout << firstName << std::endl;

	std::cout << "Last name: ";
	std::cout << lastName << std::endl;

	std::cout << "Nickname: ";
	std::cout << nickname << std::endl;

	std::cout << "Login: ";
	std::cout << login << std::endl;

	std::cout << "Postal address: ";
	std::cout << postalAdress << std::endl;

	std::cout << "Email address: ";
	std::cout << email << std::endl;

	std::cout << "Phone number: ";
	std::cout << phoneNumber << std::endl;

	std::cout << "Birthday date: ";
	std::cout << birthday << std::endl;

	std::cout << "Favorite meal: ";
	std::cout << favMeal << std::endl;

	std::cout << "Underwear color: ";
	std::cout << underwearColor << std::endl;

	std::cout << "Darkest secret: ";
	std::cout << darkestSecret << std::endl;
}
