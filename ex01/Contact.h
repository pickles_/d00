//
// Created by Dmitry Titenko on 10/30/17.
//

#ifndef D00_CONTACT_H
#define D00_CONTACT_H


#include <string>
#include <ostream>

class Contact
{
	private:
		std::string firstName,
			lastName,
			nickname,
			login,
			postalAdress,
			email,
			phoneNumber,
			birthday,
			favMeal,
			underwearColor,
			darkestSecret;

	public:
		Contact();

		void requestAll();

		const std::string &getFirstName() const;
		void setFirstName(const std::string &firstName);
		void requestFirstName();

		const std::string &getLastName() const;
		void setLastName(const std::string &lastName);
		void requestLastName();

		const std::string &getNickname() const;
		void setNickname(const std::string &nickname);
		void requestNickname();

		const std::string &getLogin() const;
		void setLogin(const std::string &login);
		void requestLogin();

		const std::string &getPostalAdress() const;
		void setPostalAdress(const std::string &postalAdress);
		void requestPostalAdress();

		const std::string &getEmail() const;
		void setEmail(const std::string &email);
		void requestEmail();

		const std::string &getPhoneNumber() const;
		void setPhoneNumber(const std::string &phoneNumber);
		void requestPhoneNumber();

		const std::string &getBirthday() const;
		void setBirthday(const std::string &birthday);
		void requestBirthday();

		const std::string &getFavMeal() const;
		void setFavMeal(const std::string &favMeal);
		void requestFavMeal();

		const std::string &getUnderwearColor() const;
		void setUnderwearColor(const std::string &underwearColor);
		void requestUnderwearColor();

		const std::string &getDarkestSecret() const;
		void setDarkestSecret(const std::string &darkestSecret);
		void requestDarkestSecret();

		void printTrunc(int id);
		void printTruncStr(const std::string &str);
		void printAll();
};


#endif //D00_CONTACT_H
